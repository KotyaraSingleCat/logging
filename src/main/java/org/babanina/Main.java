package org.babanina;

import java.io.File;
import java.util.Scanner;
import org.slf4j.*;

public class Main {
  private static final Logger log = LoggerFactory.getLogger(Main.class);

  public static void main(String[] args) throws Exception{
    Marker fileMarker = MarkerFactory.getMarker("FILE");
    Marker archiveMarker = MarkerFactory.getMarker("ARCHIVE");
    InternalData[] internalData = Data.data();
    String csvFile = "C:\\Users\\user\\IdeaProjects\\logging\\src\\main\\resources\\data_babanina.csv";
    String csvFileNew = "C:\\Users\\user\\IdeaProjects\\logging\\src\\main\\resources\\data1_babanina.csv";
    String zipPath = "C:\\Users\\user\\Desktop\\output.zip";
    log.info(fileMarker, "Start saving!");
    try {
      WriteDataInFile writeDataInFile = new WriteDataInFile(csvFile, internalData);
      writeDataInFile.writeData();
    }catch (Exception e){
      log.error(fileMarker, "IOException", e);
    }

    try {
      ReadWriteZip.write(csvFile,zipPath, "data_babanina.csv");
      log.debug("Zip completed! File name: {}", zipPath.substring(zipPath.lastIndexOf("\\") + 1));
      ReadWriteZip.read(zipPath, csvFileNew);
    } catch (Exception e){
      log.error(archiveMarker, "Exception", e);
    }

    Scanner input = new Scanner(new File(csvFile));
    while (input.hasNext()) {
        log.info(fileMarker, input.next());
    }
  }
}

