package org.babanina;

import java.io.IOException;

public interface WriteCSVFiles {
   void writeData() throws IOException;
}
