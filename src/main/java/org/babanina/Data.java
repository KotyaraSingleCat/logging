package org.babanina;

import java.time.LocalDate;

public class Data {

  public static InternalData[] data(){
    InternalData data1 = new InternalData(LocalDate.of(2020, 11, 30), 14, 25);
    InternalData data2 = new InternalData(LocalDate.of(2020, 12, 25), 4, 60);
    InternalData data3 = new InternalData(LocalDate.of(2020, 10, 10), 17, 45);
    InternalData data4 = new InternalData(LocalDate.of(2020, 9, 14), 15, 22);
    InternalData data5 = new InternalData(LocalDate.of(2020, 3, 23), 20, 38);
    InternalData[] internalData = {data1, data2, data3, data4, data5};
    return internalData;
  }
}
