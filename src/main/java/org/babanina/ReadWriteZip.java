package org.babanina;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;
import java.util.zip.ZipOutputStream;

public class ReadWriteZip {

  public static void read(String zipPath, String filePath) throws Exception{
    ZipInputStream zin = new ZipInputStream(new FileInputStream(zipPath));
      while(zin.getNextEntry()!=null){
        FileOutputStream fout = new FileOutputStream(filePath);
        for (int c = zin.read(); c != -1; c = zin.read()) {
          fout.write(c);
        }
        fout.flush();
        zin.closeEntry();
        fout.close();
      }

  }

  public static void write(String filePath, String zipPath, String fileName) throws Exception {
    ZipOutputStream zout = new ZipOutputStream(new FileOutputStream(zipPath));
    FileInputStream fis = new FileInputStream(filePath);
    ZipEntry entry = new ZipEntry(fileName);
    zout.putNextEntry(entry);
    byte[] buffer = new byte[1024];
    fis.read(buffer);
    zout.write(buffer);
    zout.closeEntry();
  }
}
