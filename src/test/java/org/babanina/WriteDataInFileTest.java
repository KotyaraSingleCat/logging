package org.babanina;

import au.com.bytecode.opencsv.CSVWriter;
import org.junit.Test;

import java.io.*;

import static org.junit.Assert.*;

public class WriteDataInFileTest {

  private WriteDataInFile writeDataInFile = new WriteDataInFile("C:\\Users\\user\\IdeaProjects\\logging\\src\\test\\resources\\test_babanina.csv", Data.data());

  @Test
  public void writeData() throws Exception {
    CSVWriter writer = new CSVWriter(new FileWriter(writeDataInFile.csvFile));
    for (InternalData internal : writeDataInFile.internalData) {
      String[] record = {internal.getDate().toString(), String.valueOf(internal.getTemperature()),
          String.valueOf(internal.getWetness())};
      writer.writeNext(record);
    }
    writer.close();

    BufferedReader br = null;
    String line = "";
    String cvsSplitBy = ",";
    String[] data = null;
    try {

      br = new BufferedReader(new FileReader("C:\\Users\\user\\IdeaProjects\\logging\\src\\test\\resources\\test_babanina.csv"));
      while ((line = br.readLine()) != null) {

        data = line.split(cvsSplitBy);
      }

    } catch (FileNotFoundException e) {
      e.printStackTrace();
    } catch (IOException e) {
      e.printStackTrace();
    } finally {
      if (br != null) {
        try {
          br.close();
        } catch (IOException e) {
          e.printStackTrace();
        }
      }
    }
    assertEquals("\"2020-03-23\"", data[0]);
    assertEquals("\"20\"", data[1]);
    assertEquals("\"38\"", data[2]);
  }
}