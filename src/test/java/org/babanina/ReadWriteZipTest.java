package org.babanina;

import org.junit.Test;

import java.io.*;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;
import java.util.zip.ZipOutputStream;

import static org.junit.jupiter.api.Assertions.*;

public class ReadWriteZipTest {

  @Test
  public void shouldReadZip() throws Exception {
    ZipInputStream zin = new ZipInputStream(new FileInputStream("C:\\Users\\user\\IdeaProjects\\logging\\src\\test\\resources\\test_data.zip"));
    while(zin.getNextEntry()!=null){
      FileOutputStream fout = new FileOutputStream("C:\\Users\\user\\IdeaProjects\\logging\\src\\test\\resources\\test_data_2.txt");
      for (int c = zin.read(); c != -1; c = zin.read()) {
        fout.write(c);
      }
      fout.flush();
      zin.closeEntry();
      fout.close();
    }

    BufferedReader fBr = new BufferedReader(new FileReader("C:\\Users\\user\\IdeaProjects\\logging\\src\\test\\resources\\test_data_2.txt"));
    BufferedReader sBr = new BufferedReader(new FileReader("C:\\Users\\user\\IdeaProjects\\logging\\src\\test\\resources\\test_data.txt"));

    try {
      int byte1;
      while((byte1 = fBr.read())!=-1) {
        int byte2 = sBr.read();
        assertEquals(byte1, byte2);
      }
    } finally {
      fBr.close();
      sBr.close();
    }
  }

  @Test
  public void shouldWriteZip() throws Exception {
    ZipOutputStream zout = new ZipOutputStream(new FileOutputStream("C:\\Users\\user\\IdeaProjects\\logging\\src\\test\\resources\\test_data_2.zip"));
    FileInputStream fis = new FileInputStream("C:\\Users\\user\\IdeaProjects\\logging\\src\\test\\resources\\test_data.txt");
    ZipEntry entry = new ZipEntry("test_data.txt");
    zout.putNextEntry(entry);
    byte[] buffer = new byte[1024];
    fis.read(buffer);
    zout.write(buffer);
    zout.closeEntry();

    FileInputStream fis1 = new FileInputStream("C:\\Users\\user\\IdeaProjects\\logging\\src\\test\\resources\\test_data.zip");
    FileInputStream fis2 = new FileInputStream("C:\\Users\\user\\IdeaProjects\\logging\\src\\test\\resources\\test_data_2.zip");
    try {
      int byte1;
      while((byte1 = fis1.read())!=-1) {
        int byte2 = fis2.read();
        assertEquals(byte1, byte2);
      }
    } finally {
      fis1.close();
      fis2.close();
    }
  }
}