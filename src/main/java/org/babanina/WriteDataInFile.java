package org.babanina;

import au.com.bytecode.opencsv.CSVWriter;

import java.io.FileWriter;
import java.io.IOException;

public class WriteDataInFile implements WriteCSVFiles{
  String csvFile;
  InternalData[] internalData;

  public WriteDataInFile(String csvFile, InternalData[] internalData) {
    this.csvFile = csvFile;
    this.internalData = internalData;
  }

  @Override
  public void writeData() throws IOException {
    CSVWriter writer = new CSVWriter(new FileWriter(csvFile));
    for (InternalData internal : internalData) {
      String[] record = {internal.getDate().toString(), String.valueOf(internal.getTemperature()),
          String.valueOf(internal.getWetness())};
      writer.writeNext(record);
    }
    writer.close();
  }
}
